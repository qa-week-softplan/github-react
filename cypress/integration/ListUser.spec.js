/// <reference types="cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit('')
  })

  it('Should render List User', () => {
    cy.wait(10000);
    cy.get('div[data-testid="user-login"]').should('have.text', 'mojombo');
  });
});