import React from "react";

import Main from "./components/Main";
import ListUser from "./screens/ListUser";

const App = () => {
  return (
    <Main>
      <ListUser />
    </Main>
  );
};

export default App;
