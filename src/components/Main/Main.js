import React from "react";

const Main = ({ children }) => {
  return (
    <div className="root" data-testid="root">
      <div className="content">{children}</div>
    </div>
  );
};

export default Main;
