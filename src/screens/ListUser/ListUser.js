import React, { useEffect, useState } from "react";

import Loading from "../../components/Loading";
import { allUsers } from "../../services/github";
import ListItem from "./components/ListItem";

const ListUser = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const data = await allUsers();
      setUsers(data);
      setLoading(false);
    };

    fetchData();
  }, []);

  if (loading) {
    return (
      <Loading>
        <h1>Carregando</h1>
      </Loading>
    );
  }

  return (
    <div className="grid grid-cols-3 gap-4">
      {users.map((item) => (
        <ListItem key={item.id} user={item} />
      ))}
    </div>
  );
};

export default ListUser;
