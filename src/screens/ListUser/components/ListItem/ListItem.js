import React from "react";

const ListItem = ({ user }) => (
  <a href={user.html_url} key={user.id}>
    <figure className="md:flex bg-gray-100 rounded-xl p-8 md:p-0">
      <img
        className="ml-0 w-32 h-32 md:w-48 md:h-auto md:rounded-none rounded-full mx-auto"
        src={user.avatar_url}
        alt=""
        width="384"
        height="512"
      />
      <div className="pt-6 md:p-8 text-center md space-y-4">
        <blockquote>
          <p className="text-lg font-semibold">
            {user.name} / {user.login}
          </p>
        </blockquote>
        <figcaption className="font-medium">
          <div className="text-cyan-600">{user.location}</div>
          <div className="text-gray-500" data-testid="user-login">
            {user.login}
          </div>
          <div>{user.blog}</div>
        </figcaption>
      </div>
    </figure>
  </a>
);

export default ListItem;
